# OpenSSL req with SAN #


### Template file "csr_details" ###
[req]  
default_bits = 2048  
prompt = no  
default_md = sha256  
req_extensions = req_ext  
distinguished_name = dn  
 
[ dn ]  
C=  
ST=  
L=  
O=  
OU=  
emailAddress=  
CN=
 
[ req_ext ]  
subjectAltName = @alt_names  

[ alt_names ]  
DNS.1 =  
DNS.2 =  


### Command line ###

openssl req -new -sha256 -nodes -out your-new-domain.com.csr -newkey rsa:2048 -keyout your-new-domain.com.key -config csr_details